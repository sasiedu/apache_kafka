#/bin/bash

echo "***** Saving ENVs to /kafka/config/server.properties"

cat /kafka/config/kafka-server.properties | sed \
	-e "s|{{KAFKA_BROKER_ID}}|$KAFKA_BROKER_ID|g" \
	-e "s|{{KAFKA_ADVERTISED_HOST_NAME}}|$KAFKA_ADVERTISED_HOST_NAME|g" \
	-e "s|{{KAFKA_PORT}}|$KAFKA_PORT|g" \
	-e "s|{{KAFKA_ADVERTISED_PORT}}|$KAFKA_ADVERTISED_PORT|g" \
	-e "s|{{KAFKA_NUM_PARTITIONS}}|$KAFKA_NUM_PARTITIONS|g" \
	-e "s|{{ZOOKEEPER_CONNECTION_STRING}}|$ZOOKEEPER_CONNECTION_STRING|g" \
> /kafka/config/server.properties

echo "***** Saving done"

echo "***** Starting kafka server with BROKER_ID=${KAFKA_BROKER_ID}"
/kafka/bin/kafka-server-start.sh /kafka/config/server.properties
