#/bin/bash

if [[ $# -eq 0 ]] ; then
	echo "Usage: ./build.sh base|server|zookeeper"
	exit 1
fi

if [[ $1 == "base" ]] ; then
	echo "***** Building kafka base image"
	docker build -f Dockerfile.java -t kafka-base .
	exit 1
fi

if [[ $1 == "server" ]] ; then
	echo "***** Building kafka server image"
	docker build -f Dockerfile.server -t kafka-server .
	exit 1
fi

if [[ $1 == "zookeeper" ]] ; then
	echo "***** Building kafka zookeeper image"
	docker build -f Dockerfile.zookeeper -t kafka-zookeeper .
	exit 1
fi

echo "No option: $1"
echo "Usage: ./build.sh base|server|zookeeper"
